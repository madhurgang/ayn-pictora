import React from "react";
import { Provider } from "react-redux";
import * as Font from "expo-font";
import { AppLoading } from "expo";
import { YellowBox} from "react-native";
import Core from "./Core";
import { Root } from "native-base";
import store from "./src/store";

YellowBox.ignoreWarnings(["Remote debugger"]);
export default class App extends React.Component {
  state = {
    isLoadingComplete: false
  };

  render() {
    if (!this.state.isLoadingComplete) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Root>
          <Provider store={store} >
            <Core />
          </Provider>
        </Root>
      );
    }
  }


  _loadResourcesAsync = async () => {
    return Promise.all([
      Font.loadAsync({
        "NotoSans-Light": require("./assets/fonts/NotoSans-Light.ttf"),
        "NotoSans-Bold": require("./assets/fonts/NotoSans-Bold.ttf"),
        "NotoSans-Regular": require("./assets/fonts/NotoSans-Regular.ttf"),
        "martel-regular": require("./assets/fonts/martel-regular.otf"),
        "martel-bold": require("./assets/fonts/martel-bold.otf")
      })
    ]);
  };

  _handleLoadingError = error => {
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({
      isLoadingComplete: true
    });
  };
}
