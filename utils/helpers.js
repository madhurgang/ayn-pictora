import { AsyncStorage } from "react-native";

export const getLocalStorageData = async key => {
  const data = await AsyncStorage.getItem(key);
  if (data) return data;
  return null;
};

deg2rad = (deg) => {
  return deg * (Math.PI / 180)
}