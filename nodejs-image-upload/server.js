const express = require('express');
const multer = require('multer');
const upload = multer({ dest: __dirname + '/uploads/images' });
const bodyParser = require('body-parser')
const app = express();
const PORT = 3000;
const fs = require('fs');
const mime = require('mime');


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static('public'));

const uploadImage = async (req, res, next) => {
    // to declare some path to store your converted image
    console.log(req.body)
    var matches = req.body.base64Img.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
    console.log('data', response.data);
    let decodedImg = response;
    let imageBuffer = decodedImg.data;
    let type = decodedImg.type;
    let extension = mime.extension(type);
    let fileName = "image." + extension;
    try {
        fs.writeFileSync("./uploads/images" + fileName, imageBuffer, 'utf8');
        return res.send({ "status": "success" });
    } catch (e) {
        console.log(e)
    }
}
app.post('/upload/image', (req, res, next) => {
    uploadImage(req, res, next)
})

app.listen(PORT, () => {
    console.log('Listening at ' + PORT);
});