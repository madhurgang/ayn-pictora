import React from "react";
import {
  StyleSheet
} from "react-native";
import { createAppContainer} from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import NavigationService from "./utils/NavigationService";
import CaptureSave from "./src/screens/Capture/CaptureSave";
import CaptureScreen from "./src/screens/Capture/CaptureScreen";
import Picture from "./src/screens/Picture";

const RootNavigation = createStackNavigator(
  {
    Picture: Picture,
    Capture: CaptureScreen,
    CaptureSave: CaptureSave,
  })


const AppContainer = createAppContainer(RootNavigation);

class Core extends React.Component {
  render() {
    return (
      <AppContainer
        style={styles.root}
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}

const mapState = state => ({});

const mapActions = dispatch => ({});

export default Core;

const styles = StyleSheet.create({
  root: {
    backgroundColor: "rgb(239, 235, 233)"
  },
  loader: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});
