export const RootReducer = (state = {}, action) => {
  switch (action.type) {
    case "CAPTURE_IMAGE": {
      return {
        ...state,
        image: action.payload
      };
    }
    default:
      return state;
  }
}