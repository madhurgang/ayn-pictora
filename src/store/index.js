import { combineReducers, applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { logger } from "redux-logger";
import { RootReducer } from "./reducer";

const reducers = combineReducers({
  rootReducer: RootReducer
});

const middleware = applyMiddleware(logger, thunk);

let store = createStore(reducers, middleware);

export default store;