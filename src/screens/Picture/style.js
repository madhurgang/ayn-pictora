import { StyleSheet } from 'react-native'
export const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    backgroundColor: '#E5E5E5'
  },
  cardWrapper: {
    backgroundColor: 'white',
    borderRadius: 14,
    marginBottom: 15
  },
  card: {
    borderRadius: 14
  },
  image: {

    height: 150
  },
  imageBackground: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
    borderRadius: 14,
    justifyContent: 'center',
    alignContent: 'center'
  },
  imageText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'NotoSans-Bold',
    color: 'white'
  },
  textContainer: {
    padding: 20,
  },
  iconText: {
    flexDirection: 'row'
  },
  iconContainer: {
    flex: 1,
    flexDirection: 'row-reverse',
    paddingTop: 10
  },
  cardText: {
    padding: 10,
    color: 'black',
    fontSize: 16
  },
  buttonContainer: {
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 14,
    height: 50
  }
})