import React from 'react';
import { SafeAreaView, View, Button } from 'react-native';
import { styles } from './style';
import { Overlay } from 'react-native-elements';
import { connect } from 'react-redux';
import CaptureScreen from '../Capture/CaptureScreen';
import NavigationService from '../../../utils/NavigationService';
import { captureImage } from '../Capture/action';


class Picture extends React.Component {

  state = {
    capture: false,
  };

  handleVisible = () => {
    const { capture } = this.state
    this.setState({ capture: !capture })
  }
  handleNavigation = () => {
    this.setState({ capture: false }, () => {
      NavigationService.navigate('CaptureSave')
    })
  }

  handlePhoto = (image) => {
    console.log('image', image);
    this.props.captureImage(image)
    this.setState({ capture: false }, () => {
      NavigationService.navigate('CaptureSave')
    })
  }

  render() {
    return (
      <SafeAreaView style={styles.root}>
        <Overlay
          isVisible={this.state.capture}
          fullScreen={true}
          windowBackgroundColor="rgba(0, 0, 0, 0.3)"
          overlayStyle={{ padding: 0 }}
        >
          <CaptureScreen handleVisible={this.handleVisible} handlePhoto={this.handlePhoto} />
        </Overlay>
        <View style={{ marginTop: 225, flex: 1 }}>
          <Button
            style={styles.buttonContainer}
            textStyles={{ color: 'green' }}
            title='Take Picture'
            onPress={() => this.handleVisible()}
          />
        </View>
      </SafeAreaView>
    )
  }
}
const mapActions = (dispatch) => ({
  captureImage: (image) => dispatch(captureImage(image))
})
const mapState = (state) => ({

})
export default connect(null, mapActions)(Picture)