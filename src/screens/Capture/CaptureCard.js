import React from "react";
import * as Permissions from 'expo-permissions';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { faMapMarkerAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-native-fontawesome";
import { View, Text, ImageBackground, TouchableOpacity } from 'react-native';
import { styles } from './style';
import { connect } from 'react-redux';

class CaptureCard extends React.Component {

  state = {
    latitude: null,
    longitude: null
  };

  componentDidMount = async () => {
    const { status } = await Permissions.askAsync(
      Permissions.LOCATION
    );
    console.log('status', status)
    if (status === 'granted') {
      navigator.geolocation.getCurrentPosition(
        ({ coords: { latitude, longitude } }) => this.setState({ latitude, longitude }, () => console.log(this.state)),
        (error) => console.log(error)
      )
    }
  }
  render() {
    const { latitude, longitude } = this.state
    console.log('render', [latitude, longitude]);
    return (
      <View>
        <KeyboardAwareScrollView enableOnAndroid={true}>
          <View style={styles.cardWrapper}>
            <View style={styles.card}>
              <ImageBackground source={{ uri: this.props.image }} imageStyle={{ borderTopLeftRadius: 14, borderTopRightRadius: 14 }} style={styles.image}>
              </ImageBackground>
            </View>
            <View style={styles.cardTextContainer}>
              <View style={styles.textContainer}>
                <TouchableOpacity style={{ paddingTop: 10 }} >
                  <FontAwesomeIcon icon={faMapMarkerAlt} color='black' size={16} />
                </TouchableOpacity>
                <Text style={styles.cardText}>
                  {latitude} {longitude}
                </Text>
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  }
}
const mapActions = (dispatch) => ({

})
const mapState = (state) => ({
})

export default connect(mapState, null)(CaptureCard)