export const captureImage = (image) => {
  return {
    type: "CAPTURE_IMAGE",
    payload: image
  }
}