import { StyleSheet } from 'react-native'
export const styles = StyleSheet.create({
  root: {
    flex: 1,
    padding: 15,
    backgroundColor: '#E5E5E5'
  },
  cardWrapper: {
    backgroundColor: 'white',
    borderRadius: 14,
    marginBottom: 15
  },
  card: {
    borderRadius: 14
  },
  image: {
    height: 240
  },
  imageBackground: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
    borderRadius: 14,
    justifyContent: 'center',
    alignContent: 'center'
  },
  imageText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'NotoSans-Bold',
    color: 'white'
  },
  cardTextContainer: {
    padding: 15,
  },
  textContainer: {
    paddingTop: 10,
    flexDirection: 'row',
  },
  cardText: {
    paddingLeft: 10,
    color: 'black',
    fontSize: 16,
  },
  buttonContainer: {
    backgroundColor: 'white',
    elevation: 5,
    borderRadius: 14,
    height: 50
  },
  locationRoot: {
    flex: 1,
    padding: 15,
    backgroundColor: '#E5E5E5',
    justifyContent: 'center',
    alignContent: 'center'
  },
  locationContainer: {
    backgroundColor: 'white',
    height: 220,
    borderRadius: 14
  },
  errorText: {
    textAlign: 'center',
    fontFamily: 'NotoSans-Bold',
    color: '#f8333c',
    marginTop: 10,
    fontSize: 16
  },
  errorInfo: {
    textAlign: 'center',
    fontSize: 16,
    marginTop: 10
  }

})