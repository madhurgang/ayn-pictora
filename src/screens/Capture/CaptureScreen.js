import React from 'react'
import { Camera } from 'expo-camera';
import * as ImageManipulator from "expo-image-manipulator";
import { View, Button } from 'react-native';
import { connect } from 'react-redux';

class CaptureScreen extends React.Component {

  state = {
    hasPermission: null,
    type: Camera.Constants.Type.back,
    capturing: false,
    focusedScreen: false
  }

  uploadToExpress = (data) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({ "base64Img": `data:image/png;base64,${data}` });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("http://2d3887f707e4.ngrok.io/upload/image", requestOptions)
      .then(response => response.text())
      .then(result => console.log(result))
      .catch(error => console.log('error', error));
  }


  componentDidMount = async () => {
    const { status } = await Camera.requestPermissionsAsync();
    console.log('status', status)
    this.setState({ hasPermission: status === "granted" }, () => {
      console.log('didMount', this.state.hasPermission)
    })
  }

  snap = async () => {
    this.setState({
      capturing: true
    });
    if (this.camera) {
      let photo = await this.camera.takePictureAsync({ skipProcessing: true, quality: 0, base64: true });
      let resizedPhoto = await ImageManipulator.manipulateAsync(
        photo.uri,
        [{ resize: { width: 500 } }],
        { compress: 0.2, format: "jpeg", base64: true }
      );
      console.log('resizedPhoto', resizedPhoto)
      this.uploadToExpress(resizedPhoto.base64)
      this.props.handlePhoto(resizedPhoto.base64)
    } else {
      alert("Please retry!");
    }
  };

  render() {
    const { hasPermission } = this.state
    if (hasPermission === null) {
      return (<View></View>)
    }
    else if (hasPermission === false) {
      return (<Text>error</Text>)
    }
    return (

      <View style={{ flex: 1 }}>
        <Camera style={{ flex: 1 }} type={this.state.type} ref={ref => { this.camera = ref }} >
          <View
            style={{
              flex: 1,
              padding: 10,
              backgroundColor: 'transparent',
              flexDirection: 'column-reverse',
            }}>

            <View style={{ padding: 20 }}>
              <Button
                style={{
                  elevation: 5,
                  borderRadius: 14
                }}

                title='Capture'
                onPress={this.snap}
              />
              <Button
                style={{
                  backgroundColor: 'white',
                  elevation: 5,
                  borderRadius: 14,
                  marginTop: 10,
                }}
                textStyles={{ color: 'red' }}
                title='Cancel'
                onPress={this.props.handleVisible}
              />
            </View>
          </View>
        </Camera>
      </View>
    );
  }
}

const mapActions = (dispatch) => ({

})
const mapState = (state) => ({
  image: state.rootReducer.image
})

export default connect(mapState, null)(CaptureScreen)