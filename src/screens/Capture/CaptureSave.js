import React from 'react'
import NetInfo from "@react-native-community/netinfo";
import { SafeAreaView, AsyncStorage, Button, ToastAndroid, Text } from 'react-native';
import { styles } from './style';
import CaptureCard from './CaptureCard';
import { connect } from 'react-redux';

class CaptureSave extends React.Component {

  handleSubmit = () => {
    const objKey = 'objKey'
    const imageObject = { 'imageId': 1, image: this.props.image }
    NetInfo.fetch().then(async state => {
      if (state.isConnected && state.isInternetReachable) {
        await AsyncStorage.setItem(objKey, JSON.stringify(imageObject))
      }
    });
  }

  showToastWithGravity = () => {
    ToastAndroid.showWithGravity(
      "image succefully uploaded",
      ToastAndroid.SHORT,
      ToastAndroid.CENTER
    ), () => {
      NavigationService.navigate('Picture')
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.root}>
        <CaptureCard
          handleSubmit={this.handleSubmit}
          image={`data:image/png;base64,${this.props.image}`}

        />
        <Button
          title="Upload"
          onPress={() => this.showToastWithGravity()}
        />
      </SafeAreaView>
    )
  }
}

const mapActions = (dispatch) => ({

})
const mapState = (state) => ({
  image: state.rootReducer.image
})

export default connect(mapState, null)(CaptureSave)